import Tab from './Tab.svelte';
import { render } from '@testing-library/svelte';

describe('Tab component', () => {

  it('should not render default slot content when not active', () => {
    const { getByTestId } = render(
      <Tab id="1" active="2">
        <h1 data-testid="slot" >Test</h1>
      </Tab>);
  
      expect(() => getByTestId('slot')).toThrow();
  });

  it('should render default slot content when active', () => {
    const { getByTestId } = render(
    <Tab id="1" active="1" >
      <h1 data-testid="slot" >Test</h1>
    </Tab>);

  expect(() => getByTestId('slot')).not.toThrow();
  expect(getByTestId('slot')).toBeInTheDocument();
  })
});
