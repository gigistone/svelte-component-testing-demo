import Button from './Button.svelte';
import { render, fireEvent } from '@testing-library/svelte';

describe('Button Component', () => {


  describe('on default', () => {
    it('should render slotted content', () => {
      const { getByTestId } = render(
        <Button>
          <h1 data-testid="slot">Test Slot</h1>
        </Button>
      );
  
      expect(() => getByTestId('slot')).not.toThrow();
      expect(getByTestId('slot')).toBeInTheDocument();
    });
  
    it('should raise click event on click', () => {
      const mockFn = jest.fn();
  
      const { getByTestId } = render(
        <Button onClick={mockFn}>
          <div data-testid="button"></div>
        </Button>
      );
  
      const btn = getByTestId('button');
      fireEvent.click(btn);
  
      expect(mockFn).toBeCalled();
      expect(mockFn).toBeCalledTimes(1);
    });
  
    it('should raise action event on click', () => {
      const mockFn = jest.fn();
  
      const { getByTestId } = render(
        <Button onAction={mockFn}>
          <div data-testid="button"></div>
        </Button>
      );
  
      const btn = getByTestId('button');
      fireEvent.click(btn);
  
      expect(mockFn).toBeCalled();
      expect(mockFn).toBeCalledTimes(1);
    });
  })


  describe('when disabled', () => {
    
      it('should not raise action event on click', () => {
        const mockFn = jest.fn();
    
        const { getByTestId } = render(
          <Button onAction={mockFn} disabled>
            <div data-testid="button"></div>
          </Button>
        );
    
        const btn = getByTestId('button');
        fireEvent.click(btn);
    
        expect(mockFn).not.toBeCalled();
      });


      it('should raise click event on click', () => {
        const mockFn = jest.fn();
    
        const { getByTestId } = render(
          <Button onClick={mockFn} disabled>
            <div data-testid="button"></div>
          </Button>
        );
    
        const btn = getByTestId('button');
        fireEvent.click(btn);
    
        expect(mockFn).toBeCalled();
        expect(mockFn).toBeCalledTimes(1);
      });
  });
});
